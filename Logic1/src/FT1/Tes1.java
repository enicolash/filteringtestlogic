package FT1;

import java.util.Scanner;

public class Tes1 {
	
	public void deret(int x) {
		int evenResult = 0;
		int oddResult = 1;
		int[] resultEven = new int[x];
		int[] resultOdd = new int[x];
		int[] output = new int[x];
		for(int i=0; i<x; i++) {
			resultEven[i] = evenResult;
			evenResult +=2;
			resultOdd[i] = oddResult;
			oddResult +=2;
			output[i] = resultEven[i] + resultOdd[i];
		}
		System.out.print("Output : ");
		for(int i=0; i<x; i++) {
			System.out.print(output[i] + " ");
		}
		
	}
	public static void main (String[] args) {
		Tes1 FT1 = new Tes1();
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan jumlah deret : ");
		int input = scan.nextInt();
		FT1.deret(input);
	}
}
