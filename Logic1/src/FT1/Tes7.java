package FT1;

import java.util.Scanner;

public class Tes7 {
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan Panjang Tali  : ");
		int z = scan.nextInt();
		System.out.print("Masukan panjang per potongan  : ");
		int x = scan.nextInt();
		int hasil = Math.round(z/x);
		int sisa = z%x;
		System.out.println("Dihasilkan tali sejumlah " + hasil + " dengan masing masing panjang " + x + " meter");
		System.out.println("dan sisa tali sepanjang " + sisa + " meter");
	}
}
