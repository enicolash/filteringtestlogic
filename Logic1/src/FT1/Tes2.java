package FT1;

import java.util.Scanner;

public class Tes2 {

	
	public static void main (String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan String  : ");
		String input = scan.nextLine();
		String[] inputSplit = input.split(" ");
		String[] ouput = new String[3];
		String[] awal = new String[3];
		String[] akhir = new String[3];
		String result = "";
		int[] panjang = new int[3];
		for(int i=0; i<inputSplit.length; i++) {
			panjang[i]=inputSplit[i].length();
			awal[i] =inputSplit[i].substring(0,1);
			akhir[i]=inputSplit[i].substring(panjang[i]-1,panjang[i]);
			ouput[i] = inputSplit[i].substring(1,panjang[i]-1);
			ouput[i] = ouput[i].replace(ouput[i] , "***");
		}
		for(int i=0; i<inputSplit.length; i++) {
			result = result + awal[i] + ouput[i] + akhir[i] + " ";
		}
		System.out.println("Ouput : " + result);
	}
}
