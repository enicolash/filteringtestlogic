package FT1;

import java.util.Scanner;

public class Tes10 {
	
	
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan input string : ");
		String input = scan.nextLine();
		String[] strSplit = input.split(" ");
		String newStr="";
		String vokal ="";
		String konsonan = "";
		for (int i=0; i<strSplit.length;i++) {
			newStr +=strSplit[i];
		}
//		newStr.equals(konsonan)
		newStr = newStr.toLowerCase();
		for (int i=0; i<newStr.length(); i++) {
			if(newStr.charAt(i) == 'a' || newStr.charAt(i) == 'i' || newStr.charAt(i) == 'u' || newStr.charAt(i) == 'e' || newStr.charAt(i) == 'o') {
				vokal+=newStr.charAt(i);
			}
			else {
				konsonan+=newStr.charAt(i);
			}
		}
		
		System.out.println("Huruf vokalnya adalah : "  + vokal);
		System.out.println("Huruf konsonannya adalah : " + konsonan);
		
	}

}
