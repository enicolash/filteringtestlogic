package FT1;

import java.util.Scanner;

public class Tes4 {
	
	public void convertWaktu(String input, String tipe) {
		String[]inputSplit =input.split(":");
		int jam = Integer.valueOf(inputSplit[0]);
		int menit =Integer.valueOf(inputSplit[1]);
		String menitStr ="";
		String jamStr = "";
		String newTipe = "";
		if(tipe.equals("AM")) {
			if(jam > 12) {
				jam -=12;
			}
			else if(jam == 12 && menit >0) {
				jam -=12;
			}
		}
		else if(tipe.equals("PM")) {
			if(jam < 12) {
				jam +=12;
			}
			else if(jam == 12 && menit >0) {
				jam -=12;
			}
		}
		else if(tipe.equals("none")) {
			if(jam < 12) {
				newTipe = "AM";
			}
			else if(jam == 12 && menit >0) {
				newTipe = "PM";
			}
			else if(jam > 12) {
				newTipe = "PM";
			}
			
		}
		
		
		
		if(jam <10) {
			jamStr = jamStr + 0 + jam;
		}
		if(menit < 10) {
			menitStr = menitStr + 0 + menit;
		}
		if(jam > 10) {
			jamStr = jamStr + jam;
		}
		if(menit > 10) {
			menitStr = menitStr + menit;
		}
		if(tipe.equals("none")) {
			tipe = newTipe;
		}
		
		
		
		System.out.println(jamStr+ ":" +menitStr+" " +tipe);
	}
	
	
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan waktu format(HH:MM)  : ");
		String input = scan.nextLine();
		System.out.print("Masukan waktu format(AM/PM/none)  : ");
		String tipe = scan.nextLine();
		Tes4 T4 = new Tes4();
		
		System.out.print("Output : ");
		T4.convertWaktu(input,tipe);
		
		
	}
}
