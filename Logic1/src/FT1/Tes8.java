package FT1;

import java.util.Scanner;

public class Tes8 {

	
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan Jumlah Pekerja Baru  : ");
		int newPekerja = scan.nextInt();
		int pekerja = 5;
		int hari = 3;
		double newHari;
		// asumsi 1 hari = 8 jam
		
		//pekerjabaru X haribaru = pekerjaLama X hariLama
		// Maka
		double gradien = ((double)pekerja)/newPekerja;
		newHari = gradien * hari;
		System.out.println("Dengan asumsi 1 hari adalah 8 jam kerja maka dalam : " + "" + (newHari*8) + " jam");
		System.out.println("Setara dengan " + newHari + " hari per jam kerja");
	}
}
