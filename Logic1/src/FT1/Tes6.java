package FT1;

import java.util.Scanner;

public class Tes6 {
	public static void main(String[] args) {
		Scanner scan = new Scanner (System.in);
		System.out.print("Masukan Batas Atasnya  : ");
		int x = scan.nextInt();
		int result = 3;
		System.out.println("Output : ");
		for(int i=0; i<x; i++) {
			if(result %2 == 0) {
				System.out.print(result + " ");
			}
			result+=3;
		}
	}
}
